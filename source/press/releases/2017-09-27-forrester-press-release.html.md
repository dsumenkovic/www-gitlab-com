---
layout: markdown_page
title: "GitLab Recognized as a Leader in Continuous Integration Tools Report by Independent Research Firm"
---

### “Best-in-class extensibility,” vision to serve enterprise-scale and integrated software development teams

**SAN FRANCISCO - September 22, 2017** - [GitLab](https://about.gitlab.com/), the leading integrated product for modern software development was [cited as a Leader in Continuous Integration](https://about.gitlab.com/2017/09/27/gitlab-leader-continuous-integration-forrester-wave/) in The Forrester WaveTM: Continuous Integration Tools, Q3 2017 report. GitLab was among the select companies that Forrester Research, Inc., a leading independent technology and market research company, invited to participate in this inaugural Continuous Integration report.

“Automation has paved the way for disruption in the enterprise, demanding new tools and practices to keep up with the market’s appetite for continuous integration and deployment (CI/CD). CI/CD best practices allow modern DevOps teams to thrive in today’s digital transformation,” said Job van der Voort, VP of Product at GitLab. “Forrester’s recognition of GitLab’s vision to serve enterprise-scale, integrated software development supports our position as a Leader in a category critically important to the future of enterprise development.”

In the Wave report, Forrester evaluated GitLab Enterprise Edition version 9.3, GitLab’s integrated tool that helps developers build and test code prior to deployment. GitLab received the highest possible score in graphical user interface (GUI), ease of installation/configuration, configuring builds and build reuse, platform support, security features, container build support, container runtime support, analytics, as well as pricing and licensing model criteria. These among the top-ranked capabilities, coupled with the [recent release of GitLab 10.0](https://about.gitlab.com/press/releases/2017-09-22-v10-press-release.html), provide modern developers with all the capabilities needed to fully embrace the benefits of DevOps, specifically, CI/CD, monitoring and Kubernetes based application development. As the report states, “GitLab’s vision is to serve enterprise-scale, integrated software development teams that want to spend more time writing code and less time maintaining their tool chain.”

”Adoption of CI tools is growing as more (application development and delivery) AD&D organizations shift to Agile and DevOps,” according to the Forrester Wave report. “[This results] in smaller, product-centric, autonomous software development teams. AD&D leaders empower their teams with CI tools that automate software delivery tasks and easily integrate with other parts of the tool chain, without unwanted complication and management overhead.” 

Forrester included vendors based on their offering of a full-featured CI solution, being able to provide evidence of customers’ successes and having generated strong interest among Forrester’s clients. Forrester further evaluated vendors against a comprehensive set of evaluation criteria, grouped into strength of strategy, current offering and market presence.

GitLab’s strengths include supporting development teams with well-documented installation and configuration processes, an intuitive UI, and a flexible per-seat pricing model that supports self service. GitLab stands apart from most other vendors in the market by making its planned enhancements road map available to the community through a public issue tracker. With the delivery of 71 consecutive monthly releases, GitLab is proof of the benefits of CI/CD.

GitLab helps organizations, ranging from Fortune 500 enterprises to one-person shops, embrace the power of the cloud by offering the most robust and only integrated modern software development lifecycle product. As the need for modern, collaborative solutions increase, enterprises including Comcast, Siemens, Ticketmaster, ING, NASDAQ, Alibaba, Bayer, NASA and Intel, have adopted GitLab to maintain pace with the demands of today’s work environment.

See [here](https://get.gitlab.com/forrester-wave-ci/) to download the official Forrester report.

**About GitLab**  
Since its incorporation in 2014, GitLab has quickly become the leading self-hosted Git repository management tool used by software development teams ranging from startups to global enterprise organizations. GitLab has since expanded its product offering to deliver an integrated source code management, code review, test/release automation, and application monitoring product that accelerates and simplifies the software development process. With one end-to-end software development product, GitLab helps teams eliminate unnecessary steps from their workflow, significantly reduce cycle time and focus exclusively on building great software. Today, more than 100,000 organizations, including Ticketmaster, ING, NASDAQ, Alibaba, Sony, and Intel; and millions of users, trust GitLab to bring their modern applications from idea to production, reliably and repeatedly.

**Media Contact**  
Nicole Plati  
gitlab@highwirepr.com  
415-963-4174 ext. 39
